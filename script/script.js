// Теоретичні питання.
/*1. Document Object Model(DOM) - програмний інтерфейс, що дозволяє працювати з усіма елементами HTML як з об'єктами: надавати їх властивостей, зчитувати та змінювати значення цих властивостей, видаляти тощо. Те, що для HTML було лише тегом, завдяки DOM стає об'єктом та набуває його характеристик, що робить web - сторінку "живою".Відповідно, ми можемо створювати та оперувати на сайті різноманітними інтерактивними інструментами.

2. Властивість innerText дозволяє додати до HTML елемента текстовий контент (наприклад, "Привіт!" між тегами div). Властивість innerHTML дозволяє розмістити в HTML елементі інший елемент з текстовим контентом (тег <p> з контентом "Привіт!" між тегами div), в т.ч. зберігає пробіли.

3. Звернутися до елемента сторінки можна кількома способами: через його клас, ідентифікатор, тег, атрибут name, місце розташування в DOM. Найбільш поширеним є querySelector() (querySelectorAll() для виведення всіх елементів за вказаним селектором). Він найбільш універсальний - можна знайти елемент за будь-яким його селектором: якщо має клас - за назвою класу, id - id, тегом.*/

// Практичне завдання.
// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000.

let paragraphs = document.querySelectorAll("p");
paragraphs.forEach(changeColor);
function changeColor(elem) {
  elem.style.backgroundColor = "#ff0000";
}

// або
// paragraphs.forEach(elem => elem.style.backgroundColor = "#ff0000");

// 2. Знайти елемент із id="optionsList", вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let element = document.getElementById("optionsList");
console.log(`Елемент із id="optionsList" ${element}. А саме:`);
console.log(element);
console.log(`Його батьківський елемент: ${element.parentElement}. А саме:`);
console.log(element.parentElement);

element = document.getElementById("optionsList");
element.hasChildNodes(); // true

console.log('Дочірні ноди елемента із id="optionsList":');
element = document.getElementById("optionsList").childNodes;
for (let i of element) {
  console.log("Нода: " + i + ", " + "тип ноди: " + typeof i);
}

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

// // Чи є елемент з класом testParagraph в документі?
// console.log(document.querySelector(".testParagraph")); // null

// const elements = document.querySelectorAll("p");
// for (let element of elements) {
// 	if (!element.matches('.testParagraph')) {
// 	    console.log(`This is mistake!`);
//   }
// } // 8 тегів p = 8 разів This is mistake!
// // - Чи є елемент з id testParagraph в документі?
// console.log(document.getElementById("testParagraph")); // є 1

let contentTestParagraph = document.getElementById("testParagraph");
contentTestParagraph.textContent = "This is a paragraph";

// // або
// let contentTestParagraph = document.getElementById("testParagraph");
// contentTestParagraph.innerText = "This is a paragraph";
// // або
// let contentTestParagraph = document.getElementById("testParagraph");
// contentTestParagraph.innerHTML = "This is a paragraph";

// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

let elementsMainHeader = document.querySelector(".main-header").children;
console.log('\n Елементи, вкладені в елемент із класом "main-header":');
console.log(elementsMainHeader);

for (let element of elementsMainHeader) {
  element.classList.add("nav-item");
}

// // або
// elementsClassMainHeader = document.querySelector(".main-header").children;
// let arrayOfElements = Array.from(elements);
// arrayOfElements.forEach(element => element.classList.add("nav-item"));

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let elementsSectionTitle = document.querySelectorAll(".section-title");
// console.log(elementsSectionTitle);
for (let element of elementsSectionTitle) {
  element.classList.remove("section-title");
}
// console.log(elementsSectionTitle);
