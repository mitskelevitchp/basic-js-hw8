Теоретичні питання.
1. Document Object Model (DOM) - програмний інтерфейс, що дозволяє працювати з усіма елементами HTML як з об'єктами: надавати їх властивостей, зчитувати та змінювати значення цих властивостей, видаляти тощо. Те, що для HTML було лише тегом, завдяки DOM стає об'єктом та набуває його характеристик, що робить web-сторінку "живою". Відповідно, ми можемо створювати та оперувати на сайті різноманітними інтерактивними інструментами.

2. Властивість innerText дозволяє додати до HTML елемента текстовий контент (наприклад, "Привіт!" між тегами div). Властивість innerHTML дозволяє розмістити в HTML елементі інший елемент з текстовим контентом (тег <p> з контентом "Привіт!" між тегами div), в т.ч. зберігає пробіли.

3. Звернутися до елемента сторінки можна кількома способами: через його клас, ідентифікатор, тег, атрибут name, місце розташування в DOM. Найбільш поширеним є querySelector() (querySelectorAll() для виведення всіх елементів за вказаним селектором). Він найбільш універсальний - можна знайти елемент за будь-яким його селектором: якщо має клас - за назвою класу, id - id, тегом.

Практичні завдання.
1. Знайти всі параграфи на сторінці (a) та встановити колір фону #ff0000 (b)

a)
let paragraphs = document.querySelectorAll("p");

b) 
paragraphs.forEach(changeColor);
function changeColor(elem) {
	elem.style.backgroundColor = "#ff0000";
}

або

b)
paragraph.forEach(elem => elem.style.backgroundColor = "#ff0000");

2. Знайти елемент із id="optionsList", вивести у консоль (a). Знайти батьківський елемент та вивести в консоль (b). Знайти дочірні ноди (c), якщо вони є, і вивести в консоль назви та тип нод (d).

a)
let element = document.getElementById("optionsList");
console.log(element);

b)
let element = document.getElementById("optionsList");
console.log(element.parentElement);

c)
let element = document.getElementById("optionsList");
element.hasChildNodes(); // true

d)
let element = document.getElementById("optionsList").childNodes;
for (let i of element) {
	console.log("Нода: " + i + ", " + "тип ноди: " + typeof i);
}

3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph.

- Чи є елемент з класом testParagraph в документі?
console.log(document.querySelector(".testParagraph")); // null

const elements = document.querySelectorAll("p");
for (let element of elements) {
	if (!element.matches('.testParagraph')) {
	    console.log(`This is mistake!`);
  }
} // 8 тегів p = 8 разів This is mistake!

- Чи є елемент з id testParagraph в документі?
console.log(document.getElementById("testParagraph")); // є 1

- встановити для елемента контент:
let paragraph = document.getElementById("testParagraph");
paragraph.textContent = "This is a paragraph";

або

let paragraph = document.getElementById("testParagraph");
paragraph.innerText = "This is a paragraph";

або

let paragraph = document.getElementById("testParagraph");
paragraph.innerHTML = "This is a paragraph";

4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль (a). Кожному з елементів присвоїти новий клас nav-item.

a)
let elements = document.querySelector(".main-header").children;
console.log(elements);

b)
let elements = document.querySelector(".main-header").children;
for (let element of elements) {
    element.classList.add("nav-item");
}
console.log(elements);

або

let elements = document.querySelector(".main-header").children;
let arrayOfElements = Array.from(elements);
arrayOfElements.forEach(element => element.classList.add("nav-item"));
console.log(arrayOfElements);

5. Знайти всі елементи із класом section-title (a). Видалити цей клас у цих елементів (b).

a)
let elements = document.querySelectorAll(".section-title");
console.log(elements);

b)
let elements = document.querySelectorAll(".section-title");
for (let i of elements) {
	i.classList.remove("section-title");
};
